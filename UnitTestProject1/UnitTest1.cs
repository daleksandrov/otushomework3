using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OtusHomeWork3;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAddAccount()
        {
            //����������� ������ IAccountService.AddAccount
            //��� ����� ��������� ����� ������ IRepository.Add �������� ������ accauntService (� ���������� ���� Account)

            var mock = new Mock<IRepository<Account>>();
            mock.Setup(rp => rp.Add(It.IsAny<Account>()));

            IAccountService accauntService = new AccountService(mock.Object);
            accauntService.AddAccount(new Account() { FirstName = "����", 
                                                        LastName = "�����������", 
                                                        BirthDate = new System.DateTime(1990, 02, 08) 
                                                    }
                                       );

            // ���������, ��� �������� ����� Add ������ ���� � ���������� Account
            mock.Verify(repository => repository.Add(It.IsAny<Account>()));
        }
    }
}
