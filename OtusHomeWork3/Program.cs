﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OtusHomeWork3
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] persons = new Person[]
            {
                new Person("Дима", 29),
                new Person("Вася", 27)
            };

            Console.WriteLine("Сериализуем массив объектов в XML:");

            OtusXmlSerializer otusXmlSerializer = new OtusXmlSerializer();
            string personsXML = otusXmlSerializer.Serialize(persons);

            Console.WriteLine(personsXML);            

            MemoryStream mStrm = new MemoryStream(Encoding.UTF8.GetBytes(personsXML));

            Console.WriteLine("Десериализуем XML:");
            OtusStreamReader<Person> people = new OtusStreamReader<Person>(mStrm, otusXmlSerializer);
            foreach( var pers in people)
            {
                Console.WriteLine(pers.ToString());
            }

            Console.WriteLine("Сортируем массив объектов:");

            PersonSorter personSorter = new PersonSorter();
            var sortedPers = personSorter.Sort<Person>(persons);

            foreach (var pers in sortedPers)
            {
                Console.WriteLine(pers.ToString());
            }

        }
    }
}
