﻿using System;
using System.Collections.Generic;

namespace OtusHomeWork3
{
    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    public interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }
}
