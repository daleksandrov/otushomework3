﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace OtusHomeWork3
{
    public interface ISerializer
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }

    public class OtusXmlSerializer : ISerializer
    {
        private static readonly XmlWriterSettings XmlWriterSettings;

        static OtusXmlSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
        }        

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(T))
              .Create();

            return serializer.Serialize(XmlWriterSettings, item);
        }

        public T Deserialize<T>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
               .UseAutoFormatting()
               .UseOptimizedNamespaces()
               .EnableImplicitTyping(typeof(T))
               .Create();

            return serializer.Deserialize<T>(stream);
        }

    }
}
