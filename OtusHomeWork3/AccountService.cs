﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Text;
using System.Text.Json.Serialization;

namespace OtusHomeWork3
{

    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> _repository;
        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (string.IsNullOrEmpty(account.FirstName))
                throw new Exception("FirstName должно быть заполнено");
            if (string.IsNullOrEmpty(account.LastName))
                throw new Exception("LastName должно быть заполнено");

            DateTime today = DateTime.Today;    
            TimeSpan age = today - account.BirthDate;     
            double ageInYears = age.TotalDays / 365;

            if (ageInYears > 18)
                _repository.Add(account);
            else
                throw new Exception("Возраст меньше 18 лет");
        }
    }
}
