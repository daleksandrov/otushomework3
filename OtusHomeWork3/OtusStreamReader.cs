﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OtusHomeWork3
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream _stream;
        private readonly ISerializer _serializer;

        public OtusStreamReader(Stream stream, ISerializer serializer)
        {
            _stream = stream ?? throw new ArgumentNullException(nameof(stream));
            _serializer = serializer;
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        public IEnumerator<T> GetEnumerator()
        {
            T[] arrayObj = _serializer.Deserialize<T[]>(_stream);

            foreach(var obj in arrayObj)
            {
                yield return obj;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
