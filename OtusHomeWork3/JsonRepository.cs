﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace OtusHomeWork3
{
    public class JsonRepository : IRepository<Account>
    {
        string path = "accountrepository.ndjson";
        public void Add(Account item)
        {      
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(JsonSerializer.Serialize(item));
            }
        }

        public IEnumerable<Account> GetAll()
        {
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    var accaunt = JsonSerializer.Deserialize<Account>(sr.ReadLine());
                    if(accaunt != null)
                        yield return accaunt;
                }
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }
    }
}
